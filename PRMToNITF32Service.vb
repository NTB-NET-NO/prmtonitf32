Imports System.ServiceProcess
Imports System.text
Imports System.IO
Imports ntb_FuncLib

Public Class PRMToNITF32
    Inherits System.ServiceProcess.ServiceBase

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New PRMToNITF32}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    Friend WithEvents PollTimer As System.Timers.Timer
    Friend WithEvents FileSystemWatcherPluss As System.IO.FileSystemWatcher
    Friend WithEvents FileSystemWatcherBWI As System.IO.FileSystemWatcher
    Friend WithEvents FileSystemWatcherPRS As System.IO.FileSystemWatcher
    Friend WithEvents FileSystemWatcherEquityStory As System.IO.FileSystemWatcher
    Friend WithEvents FileSystemWatcherOCDF As System.IO.FileSystemWatcher
    Friend WithEvents FileSystemWatcherOMX As System.IO.FileSystemWatcher

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.FileSystemWatcherPluss = New System.IO.FileSystemWatcher
        Me.PollTimer = New System.Timers.Timer
        Me.FileSystemWatcherBWI = New System.IO.FileSystemWatcher
        Me.FileSystemWatcherPRS = New System.IO.FileSystemWatcher
        Me.FileSystemWatcherEquityStory = New System.IO.FileSystemWatcher
        Me.FileSystemWatcherOCDF = New System.IO.FileSystemWatcher
        Me.FileSystemWatcherOMX = New System.IO.FileSystemWatcher
        CType(Me.FileSystemWatcherPluss, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileSystemWatcherBWI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileSystemWatcherPRS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileSystemWatcherEquityStory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileSystemWatcherOCDF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileSystemWatcherOMX, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'FileSystemWatcherPluss
        '
        Me.FileSystemWatcherPluss.EnableRaisingEvents = True
        Me.FileSystemWatcherPluss.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 1000
        '
        'FileSystemWatcherBWI
        '
        Me.FileSystemWatcherBWI.EnableRaisingEvents = True
        Me.FileSystemWatcherBWI.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        '
        'FileSystemWatcherPRS
        '
        Me.FileSystemWatcherPRS.EnableRaisingEvents = True
        Me.FileSystemWatcherPRS.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        '
        'FileSystemWatcherEquityStory
        '
        Me.FileSystemWatcherEquityStory.EnableRaisingEvents = True
        Me.FileSystemWatcherEquityStory.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        '
        'FileSystemWatcherOCDF
        '
        Me.FileSystemWatcherOCDF.EnableRaisingEvents = True
        Me.FileSystemWatcherOCDF.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        '
        'FileSystemWatcherOMX
        '
        Me.FileSystemWatcherOMX.EnableRaisingEvents = True
        Me.FileSystemWatcherOMX.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        '
        'PRMToNITF32
        '
        Me.CanShutdown = True
        Me.ServiceName = "NTB_PRMToNITF32"
        CType(Me.FileSystemWatcherPluss, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileSystemWatcherBWI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileSystemWatcherPRS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileSystemWatcherEquityStory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileSystemWatcherOCDF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileSystemWatcherOMX, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Vars
    Private logPath As String
    Private inputPathPluss As String
    Private inputPathBWI As String
    Private inputPathPRS As String
    Private inputPathEquityStory As String
    Private inputPathOCDF As String
    Private inputPathOMX As String
    Private outputPath As String
    Private donePath As String
    Private errorPath As String

    Protected IPTC_Sequence As Integer
    Protected IPTC_SequenceFile As String
    Protected IPTC_Range As String

    Private pollInterval As Integer

    Private plussXSLT As String
    Private BWIXSLT As String
    Private PRSXSLT As String
    Private EquityStoryXSLT As String
    Private OMXXSLT As String

    'Initiate the jobs
    Protected Sub Init()
        logPath = My.Settings.LogPath
        inputPathPluss = My.Settings.InputPathPluss
        inputPathBWI = My.Settings.InputPathBWI
        inputPathPRS = My.Settings.InputPathPRS
        inputPathEquityStory = My.Settings.InputPathEquityStory
        inputPathOCDF = My.Settings.InputPathOCDF
        inputPathOMX = My.Settings.InputPathOMX
        outputPath = My.Settings.OutputPath
        donePath = My.Settings.DonePath
        errorPath = My.Settings.ErrorPath

        LogFile.WriteLogNoDate(logPath, "---------------------------------------------------------------------------------")
        LogFile.WriteLog(logPath, "PRM Converter initiating...")

        pollInterval = My.Settings.PollingInterval

        IPTC_Range = My.Settings.IPTCRange
        IPTC_SequenceFile = My.Settings.IPTCSequenceFile

        IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        LoadSequence(IPTC_SequenceFile, True)

        LogFile.MakePath(logPath)
        LogFile.MakePath(inputPathPluss)
        LogFile.MakePath(inputPathBWI)
        LogFile.MakePath(inputPathPRS)
        LogFile.MakePath(inputPathEquityStory)
        LogFile.MakePath(inputPathOCDF)
        LogFile.MakePath(inputPathOMX)
        LogFile.MakePath(outputPath)
        LogFile.MakePath(donePath)
        LogFile.MakePath(errorPath)

        'Pluss' NITF specifics
        plussXSLT = My.Settings.PlussXSLT
        PRSXSLT = My.Settings.PRSXSLT
        EquityStoryXSLT = My.Settings.EquityStoryXSLT
        BWIXSLT = My.Settings.BWIXSLT
        OMXXSLT = My.Settings.OMXXSLT


    End Sub

    Private Sub LoadSequence(ByVal file As String, ByVal load As Boolean)
        Try
            If load Then
                IPTC_Sequence = Convert.ToInt32(LogFile.ReadFile(file).Trim())
                LogFile.WriteLog(logPath, "IPTC Sequence number loaded: " & IPTC_Sequence)
            Else
                LogFile.WriteFile(file, Convert.ToInt32(IPTC_Sequence), False)
                LogFile.WriteLog(logPath, "IPTC Sequence number saved: " & IPTC_Sequence)
            End If
        Catch ex As Exception
            LogFile.WriteErr(logPath, "Error accessing IPTC sequence number.", ex)
        End Try
    End Sub

    Private Function Get_Seq() As Integer

        'Check value
        If IPTC_Sequence > CInt(IPTC_Range.Split("-")(1)) Or _
           IPTC_Sequence < CInt(IPTC_Range.Split("-")(0)) Then
            IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        End If

        'Return value
        Dim ret As Integer = IPTC_Sequence

        'Increment
        IPTC_Sequence += 1

        'Save
        LoadSequence(IPTC_SequenceFile, False)

        Return ret
    End Function

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Init()

        PollTimer.Enabled = True
        FileSystemWatcherPluss.Path = inputPathPluss
        FileSystemWatcherPluss.EnableRaisingEvents = True

        FileSystemWatcherBWI.Path = inputPathBWI
        FileSystemWatcherBWI.EnableRaisingEvents = True

        FileSystemWatcherPRS.Path = inputPathPRS
        FileSystemWatcherPRS.IncludeSubdirectories = True
        FileSystemWatcherPRS.EnableRaisingEvents = True

        FileSystemWatcherEquityStory.Path = inputPathEquityStory
        FileSystemWatcherEquityStory.IncludeSubdirectories = True
        FileSystemWatcherEquityStory.EnableRaisingEvents = True

        FileSystemWatcherOCDF.Path = inputPathOCDF
        FileSystemWatcherOCDF.IncludeSubdirectories = True
        FileSystemWatcherOCDF.EnableRaisingEvents = True

        FileSystemWatcherOMX.Path = inputPathOMX
        FileSystemWatcherOMX.IncludeSubdirectories = True
        FileSystemWatcherOMX.EnableRaisingEvents = True

        LogFile.WriteLog(logPath, "PRM Converter started.")
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        PollTimer.Enabled = False
        FileSystemWatcherPluss.EnableRaisingEvents = False
        FileSystemWatcherBWI.EnableRaisingEvents = False
        FileSystemWatcherPRS.EnableRaisingEvents = False
        FileSystemWatcherEquityStory.EnableRaisingEvents = False
        FileSystemWatcherOCDF.EnableRaisingEvents = False
        FileSystemWatcherOMX.EnableRaisingEvents = False

        LoadSequence(IPTC_SequenceFile, False)
        LogFile.WriteLog(logPath, "PRM Converter stopped.")
    End Sub

    Protected Overrides Sub OnShutdown()
        OnStop()
    End Sub

    'Timer
    Private Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        'Stop events while handling files
        PollTimer.Enabled = False
        FileSystemWatcherPluss.EnableRaisingEvents = False
        FileSystemWatcherBWI.EnableRaisingEvents = False
        FileSystemWatcherPRS.EnableRaisingEvents = False
        FileSystemWatcherEquityStory.EnableRaisingEvents = False
        FileSystemWatcherOCDF.EnableRaisingEvents = False
        FileSystemWatcherOMX.EnableRaisingEvents = False

        'Do waiting files
        DoAllFiles()

        'Restart events
        PollTimer.Interval = pollInterval * 1000
        PollTimer.Enabled = True
        FileSystemWatcherPluss.EnableRaisingEvents = True
        FileSystemWatcherBWI.EnableRaisingEvents = True
        FileSystemWatcherPRS.EnableRaisingEvents = True
        FileSystemWatcherEquityStory.EnableRaisingEvents = True
        FileSystemWatcherOCDF.EnableRaisingEvents = True
        FileSystemWatcherOMX.EnableRaisingEvents = True
    End Sub

    'Filesystem watcher
    Private Sub FileSystemWatcher_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcherPluss.Created
        'Stop events while handling files
        PollTimer.Enabled = False
        FileSystemWatcherPluss.EnableRaisingEvents = False
        FileSystemWatcherBWI.EnableRaisingEvents = False
        FileSystemWatcherPRS.EnableRaisingEvents = False
        FileSystemWatcherEquityStory.EnableRaisingEvents = False
        FileSystemWatcherOCDF.EnableRaisingEvents = False
        FileSystemWatcherOMX.EnableRaisingEvents = False

        'Do waiting files
        DoAllFiles()

        'Restart events
        PollTimer.Enabled = True
        FileSystemWatcherPluss.EnableRaisingEvents = True
        FileSystemWatcherBWI.EnableRaisingEvents = True
        FileSystemWatcherPRS.EnableRaisingEvents = True
        FileSystemWatcherEquityStory.EnableRaisingEvents = True
        FileSystemWatcherOCDF.EnableRaisingEvents = True
        FileSystemWatcherOMX.EnableRaisingEvents = True
    End Sub

    'Loop all files
    Private Sub DoAllFiles()

        'Wait for files to be written
        Threading.Thread.Sleep(2000)

        Dim file As String
        Dim files As String() = Directory.GetFiles(inputPathPluss)

        'Loop the files
        For Each file In files
            DoOneFile(file, "(Pluss)")
        Next

        files = Directory.GetFiles(inputPathBWI)
        For Each file In files
            If file.EndsWith(".xml") Then
                'BusinessWire in NewsML format
                DoOneFile(file, "(BWIXSLT)")
            Else
                DoOneFile(file, "(BWI)")
            End If
        Next

        files = Directory.GetFiles(inputPathEquityStory)
        For Each file In files
            DoOneFile(file, "(EquityStory)")
        Next

        files = Directory.GetFiles(inputPathOCDF)
        For Each file In files
            If (Not file.EndsWith("FixedData.txt")) Then
                DoOneFile(file, "(OCDF)")
            End If
        Next

        files = Directory.GetFiles(inputPathOMX)
        For Each file In files
            DoOneFile(file, "(OMX)")
        Next

        Dim subDir As String
        Dim subDirs As String() = Directory.GetDirectories(inputPathPRS)
        For Each subDir In subDirs
            files = Directory.GetFiles(subDir)
            For Each file In files
                DoOneFile(file, "(PRS)")
            Next
            Try
                Directory.Delete(subDir)
                LogFile.WriteLog(logPath, "Slettet: '" & subDir & "'")
            Catch ex As Exception
                LogFile.WriteErr(logPath, "Sletting av katalog feilet: '" & subDir & "'", ex)
            End Try
        Next
        files = Directory.GetFiles(inputPathPRS)
        For Each file In files
            DoOneFile(file, "(PRS)")
        Next
    End Sub

    'Convert one file
    Private Sub DoOneFile(ByVal filename As String, ByVal type As String)

        Dim ok As Boolean = True
        Dim oldfile As String = filename
        Dim outfile As String = outputPath & "\" & Path.GetFileNameWithoutExtension(filename) & ".xml"
        Dim nitf As String = ""

        Try
            'Convert file
            'If filename.IndexOf(".xml") > -1 Then
            If type = "(Pluss)" Then
                'type = "(Pluss)"
                Dim conv As PlussConverter = New PlussConverter(plussXSLT)
                nitf = conv.ConvertPlussFile2NITF(filename, Get_Seq())
                'ElseIf filename.IndexOf("\\na") > -1 Then
                '   type = "(NWA)"
                '  nitf = NWAConverter.ConvertNWAFile2NITF(filename, Get_Seq())
            ElseIf type = "(BWI)" Then
                'type = "(BWI)"
                nitf = BWConverter.ConvertBWFile2NITF(filename, Get_Seq())
                outfile = outputPath & "\" & filename & ".xml"
            ElseIf type = "(BWIXSLT)" Then
                'type = "(BWIXSLT)"
                Dim conv As PRSConverter = New PRSConverter(BWIXSLT)
                nitf = conv.ConvertPRSFile2NITF(filename, Get_Seq(), logPath)
            ElseIf type = "(PRS)" Then
                'type = "(PRS)"
                Dim conv As PRSConverter = New PRSConverter(PRSXSLT)
                nitf = conv.ConvertPRSFile2NITF(filename, Get_Seq(), logPath)
            ElseIf type = "(EquityStory)" Then
                'type = "(EquityStory)"
                Dim conv As EquityStoryConverter = New EquityStoryConverter(EquityStoryXSLT)
                nitf = conv.ConvertEquityStory2NITF(filename, Get_Seq())
            ElseIf type = "(OCDF)" Then
                'type = "(OCDF)"
                Dim conv As OCDFConverter = New OCDFConverter
                nitf = conv.ConvertOCDF2NITF(filename, Get_Seq())
                outfile = outputPath & "\" & filename & ".xml"
            ElseIf type = "(OMX)" Then
                'type = "(OMX)"
                Dim conv As PRSConverter = New PRSConverter(OMXXSLT)
                nitf = conv.ConvertPRSFile2NITF(filename, Get_Seq(), logPath)
            End If

            'Dump to disk
            ' Jus tdo something here so that we see the value of nitf
            LogFile.WriteLog(logPath, nitf.ToString)
            Dim sw As StreamWriter = New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
            sw.WriteLine(nitf)
            sw.Close()

        Catch ex As Exception
            Try
                File.Copy(oldfile, errorPath & "\" & Path.GetFileName(oldfile), True)
                File.Delete(oldfile)
            Catch
            End Try

            LogFile.WriteErr(logPath, "Konvertering feilet " & type & ": '" & oldfile & "'", ex)
            ok = False
        End Try

        'Copy/delete file
        If ok Then
            LogFile.WriteLog(logPath, "PRM dokument konvertert" & type & ": '" & filename & "'")

            Try
                Dim donefile As String = donePath & "\" & Path.GetFileName(oldfile)
                donefile = FuncLib.MakeSubDirDate(donefile, File.GetLastWriteTime(oldfile))

                File.Copy(oldfile, donefile, True)
                File.Delete(oldfile)

                LogFile.WriteLog(logPath, "Slettet: '" & oldfile & "'")
            Catch ex As Exception
                LogFile.WriteErr(logPath, "Sletting feilet: '" & oldfile & "'", ex)
            End Try
        End If

    End Sub
End Class