Imports ntb_FuncLib
Imports System.io
Imports System.Text
Imports System.Text.RegularExpressions

Public Class NWAConverter

    Public Shared Function ConvertNWAFile2NITF(ByVal f As String, ByVal iptc As Integer) As String

        'Content
        Dim head As String
        Dim content As String = LogFile.ReadFile(f)
        Dim timestamp As DateTime = File.GetLastWriteTime(f)

        content = Regex.Replace(content, "[" & Chr(13) & "]+", Chr(13))
        content = Regex.Replace(content, "[" & Chr(10) & "]+", Chr(10))

        Dim pgpsTmp As String()
        pgpsTmp = Regex.Split(content, vbCrLf & vbCrLf & vbCrLf)

        head = pgpsTmp(0)
        content = pgpsTmp(1)

        Dim pgps As ArrayList = New ArrayList
        pgps.AddRange(Regex.Split(content, vbCrLf & vbCrLf))

        Dim i As Integer
        For i = 2 To pgpsTmp.GetUpperBound(0)
            pgps.AddRange(Regex.Split(pgpsTmp(i), vbCrLf & vbCrLf))
        Next

        Dim title As String = "PRM: NWA-" & MakeXMLElement(head.Substring(head.LastIndexOf("Source:") + 7))

        Dim so As String = head.Substring(head.LastIndexOf("Source:") + 7)
        so = MakeXMLElement(so)
        so = Regex.Replace(so, "[ ,.]+", "-")
        so = so.Trim("-")
        so = "PRM: NWA-" & so

        Dim header As String = MakeNITF32Header(Path.GetFileNameWithoutExtension(f), timestamp, title, so, content.Length + title.Length, iptc)
        Dim output As StringBuilder = New StringBuilder

        output.Append("<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & vbCrLf)

        output.Append("<!--<!DOCTYPE nitf SYSTEM ""nitf-3-2.dtd"">-->" & vbCrLf)
        output.Append("<nitf version=""-//IPTC//DTD NITF 3.2//EN"" change.date=""October 10, 2003"" change.time=""19:30"" baselang=""no-NO"">" & vbCrLf)

        output.Append(header)

        output.Append("<body>" & vbCrLf)

        output.Append("<body.head>" & vbCrLf)
        output.Append("<hedline><hl1>" & title & "</hl1></hedline>" & vbCrLf)
        output.Append("<distributor><org>NTB</org></distributor>" & vbCrLf)
        output.Append("</body.head>" & vbCrLf)

        output.Append("<body.content>" & vbCrLf)

        output.Append("<p lede=""true"">" & MakeXMLElement(pgps(0)) & "</p>" & vbCrLf)

        For i = 1 To pgps.Count - 1
            output.Append("<p>" & MakeXMLElement(pgps(i)) & "</p>" & vbCrLf)
        Next

        output.Append("</body.content>" & vbCrLf)

        output.Append("</body>" & vbCrLf)
        output.Append("</nitf>" & vbCrLf)

        Return output.ToString
    End Function

    Private Shared Function MakeNITF32Header(ByVal fn As String, ByVal dt As DateTime, ByVal title As String, ByVal so As String, ByVal length As Integer, ByVal iptc As Integer) As String

        Dim output As StringBuilder = New StringBuilder

        Dim id As String = "NWA:NTB:" & dt.ToString("yyyyMMdd:mmss:") & length Mod 1000

        output.Append("<head>" & vbCrLf)
        output.Append("<title>" & title & "</title>" & vbCrLf)

        output.Append("<meta name=""timestamp"" content=""" & dt.ToString("yyyy.MM.dd HH:mm:ss") & """ />" & vbCrLf)

        output.Append("<meta name=""subject"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""foldername"" content=""Ut-Satellitt"" />" & vbCrLf)
        output.Append("<meta name=""filename"" content=""" & fn & ".xml"" />" & vbCrLf)

        output.Append("<meta name=""NTBTjeneste"" content=""Formidlingstjenester"" />" & vbCrLf)
        output.Append("<meta name=""NTBMeldingsSign"" content=""nwa/"" />" & vbCrLf)
        output.Append("<meta name=""NTBPrioritet"" content=""6"" />" & vbCrLf)
        output.Append("<meta name=""NTBStikkord"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""NTBDistribusjonsKode"" content=""ALL"" />" & vbCrLf)
        output.Append("<meta name=""NTBKanal"" content=""C"" />" & vbCrLf)
        output.Append("<meta name=""NTBIPTCSequence"" content=""" & iptc & """ />" & vbCrLf)
        output.Append("<meta name=""NTBID"" content=""" & id & """ />" & vbCrLf)

        output.Append("<meta name=""ntb-dato"" content=""" & dt.ToString("dd.MM.yyyy HH:mm") & """ />" & vbCrLf)

        output.Append("<tobject tobject.type=""PRM-NA"">" & vbCrLf)
        output.Append("<tobject.property tobject.property.type=""Fulltekstmeldinger"" />" & vbCrLf)
        output.Append("</tobject>" & vbCrLf)

        output.Append("<docdata>" & vbCrLf)
        output.Append("<doc-id regsrc=""NTB"" id-string=""" & id & """/>" & vbCrLf)
        output.Append("<urgency ed-urg=""6""/>" & vbCrLf)
        output.Append("<date.issue norm=""" & dt.ToString("yyyy-MM-ddTHH:mm:ss") & """/>" & vbCrLf)
        output.Append("<ed-msg info="""" />" & vbCrLf)
        output.Append("<du-key version=""1"" key=""" & so & """ />" & vbCrLf)
        output.Append("<doc.copyright year=""" & dt.ToString("yyyy") & """ holder=""NTB"" /> " & vbCrLf)
        output.Append("<key-list>" & vbCrLf)
        output.Append("<keyword key=""" & so & """ />" & vbCrLf)
        output.Append("</key-list>" & vbCrLf)
        output.Append("</docdata>" & vbCrLf)

        output.Append("<pubdata date.publication=""" & dt.ToString("yyyyMMddTHHmmss") & """ item-length=""" & length & """ unit-of-measure=""character"" />" & vbCrLf)

        output.Append("<revision-history name=""nwa/"" />" & vbCrLf)

        output.Append("</head>" & vbCrLf)

        Return output.ToString()

    End Function

    Private Shared Function MakeXMLElement(ByVal inn As String) As String

        Dim out As String = inn.Trim()
        out = Regex.Replace(out, " +", " ")
        out = Regex.Replace(out, "[" & Chr(10) & Chr(13) & "]+", " ")

        out = out.Replace("&", "&amp;")
        out = out.Replace("<", "&lt;")
        out = out.Replace(">", "&gt;")
        out = out.Replace("""", "&quot;")

        Return out
    End Function
End Class
