Imports System.IO


Module DebugModule

    Sub main()

        Dim file, outfile As String
        Dim files As String() = Directory.GetFiles(My.Settings.InputPath)


        'Loop the files
        For Each file In files
            Dim nitf As String = BWConverter.ConvertBWFile2NITF(file, 1234)
            outfile = My.Settings.OutputPath & "\" & Path.GetFileNameWithoutExtension(file) & ".xml"

        Next

    End Sub

End Module
