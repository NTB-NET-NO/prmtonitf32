<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="ISO-8859-1" indent="yes" />

<xsl:param name="filename">
</xsl:param>

<xsl:param name="iptc_seq">
</xsl:param>

<xsl:template match="nitf">
	<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
	<xsl:apply-templates select="head"/>
	<xsl:copy-of select="body"/>
	</nitf>
</xsl:template>

<xsl:template match="head">
	<head>
	
	<xsl:copy-of select="title"/>
	<xsl:copy-of select="meta"/>

	<meta name="NTBIPTCSequence">
	<xsl:attribute name="content">
	<xsl:value-of select="$iptc_seq"/>
	</xsl:attribute>
	</meta>

	<xsl:copy-of select="tobject"/>
	<xsl:copy-of select="docdata"/>
	
	<xsl:copy-of select="pubdata"/>
	<xsl:copy-of select="revision-history"/>
	
	</head>
	
</xsl:template>

</xsl:stylesheet>