<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="ISO-8859-1" indent="yes" />

<xsl:param name="filename">
</xsl:param>

<xsl:param name="iptc_seq">
</xsl:param>

<xsl:variable name="timestamp" select="/nitf/head/pubdata/@date.publication"/>

<xsl:variable name="date-issue.norm">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 5, 2)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 7, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 10, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 14, 2)" />
</xsl:variable>

<xsl:variable name="meta-ts">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 5, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 7, 2)" />
		<xsl:text> </xsl:text>
	<xsl:value-of select="substring($timestamp, 10, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 14, 2)" />
</xsl:variable>

<xsl:variable name="ntb-dato">
	<xsl:value-of select="substring($timestamp, 7, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 5, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text> </xsl:text>
	<xsl:value-of select="substring($timestamp, 10, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
</xsl:variable>

<xsl:template match="/">
	<xsl:apply-templates select="nitf"/>
</xsl:template>

<xsl:template match="nitf">
	<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
	<xsl:apply-templates select="head"/>
	<xsl:apply-templates select="body"/>
	</nitf>
</xsl:template>

<xsl:template match="head">
	<head>
	
	<xsl:copy-of select="title"/>
	
	<meta name="timestamp">
	<xsl:attribute name="content">
	<xsl:value-of select="$meta-ts"/>
	</xsl:attribute>
	</meta>

	<meta name="subject">
	<xsl:attribute name="content">
	<xsl:value-of select="docdata/du-key/@key"/>
	</xsl:attribute>
	</meta>

	<meta name="foldername">
	<xsl:attribute name="content">
	<xsl:text>Ut-Satellitt</xsl:text>
	</xsl:attribute>
	</meta>

	<meta name="filename">
	<xsl:attribute name="content">
	<xsl:value-of select="$filename"/>
	</xsl:attribute>
	</meta>

	<meta name="NTBTjeneste" content="Formidlingstjenester" />

	<meta name="NTBMeldingsSign">
	<xsl:attribute name="content">
	<xsl:value-of select="revision-history/@name"/>
	</xsl:attribute>
	</meta>

	<meta name="NTBPrioritet">
	<xsl:attribute name="content">
	<xsl:value-of select="docdata/urgency/@ed-urg"/>
	</xsl:attribute>
	</meta>

	<meta name="NTBStikkord">
	<xsl:attribute name="content">
	<xsl:value-of select="docdata/du-key/@key"/>
	</xsl:attribute>
	</meta>

	<meta name="NTBDistribusjonsKode" content="ALL"/>
	<meta name="NTBKanal" content="C"/>

	<meta name="NTBIPTCSequence">
	<xsl:attribute name="content">
	<xsl:value-of select="$iptc_seq"/>
	</xsl:attribute>
	</meta>

	<meta name="NTBID">
	<xsl:attribute name="content">
	<xsl:value-of select="docdata/doc-id/@id-string"/>
	</xsl:attribute>
	</meta>

	<meta name="ntb-dato">
	<xsl:attribute name="content">
	<xsl:value-of select="$ntb-dato"/>
	</xsl:attribute>
	</meta>

	<tobject tobject.type="PRM-NTB">
		<tobject.property tobject.property.type="Fulltekstmeldinger"/>
	</tobject>

	<!--xsl:copy-of select="tobject"/-->
	
	<xsl:apply-templates select="docdata"/>
	
	<xsl:copy-of select="pubdata"/>
	<xsl:copy-of select="revision-history"/>
	
	</head>
</xsl:template>

<xsl:template match="docdata">
	<docdata>
	<xsl:copy-of select="doc-id"/>
	<xsl:copy-of select="urgency"/>
	
	<date.issue>
		<xsl:attribute name="norm">
		<xsl:value-of select="$date-issue.norm"/>
		</xsl:attribute>
	</date.issue>

	<xsl:copy-of select="ed-msg"/>
	<xsl:copy-of select="du-key"/>
	<xsl:copy-of select="doc.copyright"/>
	<xsl:copy-of select="key-list"/>
	</docdata>
</xsl:template>

<xsl:template match="body">
	<body>
	<xsl:apply-templates select="body.head"/>
	<xsl:copy-of select="body.content"/>
	</body>
</xsl:template>

<xsl:template match="body.head">
	<body.head>
	<xsl:copy-of select="hedline"/>
	<distributor><org>NTB</org></distributor>
	</body.head>
</xsl:template>

</xsl:stylesheet>