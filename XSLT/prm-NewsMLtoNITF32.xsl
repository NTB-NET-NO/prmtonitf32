<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="urn:NTBIPTCCodeLookup" extension-element-prefixes="ntb">
	<xsl:output method="xml" encoding="iso-8859-1" indent="yes"/>
	<xsl:param name="filename">
	</xsl:param>
	<xsl:param name="iptc_seq">
	</xsl:param>
	<xsl:param name="containsCDATA">
	</xsl:param>
	<xsl:variable name="URLToImages">
		<!--<xsl:text>http://pressrelease-ntb.belga.be/publishingntbweb/load-file.do?</xsl:text>-->
		<xsl:text>http://www.ntbinfo.no/load-file.do?</xsl:text>
	</xsl:variable>
	<xsl:variable name="URLToAttachments">
		<!--<xsl:text>http://pressrelease-ntb.belga.be/publishingntbweb/load-file.do?</xsl:text>-->
		<xsl:text>http://www.ntbinfo.no/load-file.do?</xsl:text>
	</xsl:variable>
	<xsl:variable name="timestamp" select="/NewsML/NewsEnvelope/DateAndTime"/>
	<xsl:variable name="date-issue.norm">
		<xsl:value-of select="substring($timestamp, 1, 4)"/>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="substring($timestamp, 5, 2)"/>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="substring($timestamp, 7, 2)"/>
		<xsl:text>T</xsl:text>
		<xsl:value-of select="substring($timestamp, 10, 2)"/>
		<xsl:text>:</xsl:text>
		<xsl:value-of select="substring($timestamp, 12, 2)"/>
		<xsl:text>:</xsl:text>
		<xsl:value-of select="substring($timestamp, 14, 2)"/>
	</xsl:variable>
	<xsl:variable name="meta-ts">
		<xsl:value-of select="substring($timestamp, 1, 4)"/>
		<xsl:text>.</xsl:text>
		<xsl:value-of select="substring($timestamp, 5, 2)"/>
		<xsl:text>.</xsl:text>
		<xsl:value-of select="substring($timestamp, 7, 2)"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring($timestamp, 10, 2)"/>
		<xsl:text>:</xsl:text>
		<xsl:value-of select="substring($timestamp, 12, 2)"/>
		<xsl:text>:</xsl:text>
		<xsl:value-of select="substring($timestamp, 14, 2)"/>
	</xsl:variable>
	<xsl:variable name="ntb-dato">
		<xsl:value-of select="substring($timestamp, 7, 2)"/>
		<xsl:text>.</xsl:text>
		<xsl:value-of select="substring($timestamp, 5, 2)"/>
		<xsl:text>.</xsl:text>
		<xsl:value-of select="substring($timestamp, 1, 4)"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring($timestamp, 10, 2)"/>
		<xsl:text>:</xsl:text>
		<xsl:value-of select="substring($timestamp, 12, 2)"/>
	</xsl:variable>
	<xsl:variable name="ntb-stikkord">
		<xsl:text>PRM-</xsl:text>
		<xsl:choose>
			<xsl:when test="/NewsML/NewsItem/NewsComponent/AdministrativeMetadata/Creator/Party[@FormalName='ForOrganisation']/@Value">
				<xsl:value-of select="/NewsML/NewsItem/NewsComponent/AdministrativeMetadata/Creator/Party[@FormalName='ForOrganisation']/@Value"/>
			</xsl:when>
			<xsl:when test="/NewsML/NewsItem/NewsComponent/AdministrativeMetadata/Creator/Party[@FormalName='Organisation']/@Value">
				<xsl:value-of select="/NewsML/NewsItem/NewsComponent/AdministrativeMetadata/Creator/Party[@FormalName='Organisation']/@Value"/>
			</xsl:when>
      <xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']">
        <xsl:text>BW</xsl:text>
      </xsl:when>
      <xsl:when test="/NewsML/NewsItem/Identification/NewsIdentifier[ProviderId='omxgroup.com']">
        <xsl:text>OMX</xsl:text>
      </xsl:when>
      <xsl:otherwise>
			  <xsl:text>NTB</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="/NewsML/NewsItem/Identification/NewsIdentifier/NewsItemId"/>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:if test="/NewsML/NewsEnvelope/SentTo/Party[@FormalName='NTB wire'] or /NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire'] or /NewsML/NewsItem/Identification/NewsIdentifier[ProviderId='omxgroup.com']">
			<xsl:apply-templates select="NewsML"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="NewsML">
		<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
			<xsl:apply-templates select="NewsItem"/>
		</nitf>
	</xsl:template>
	<xsl:template match="NewsItem">
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']">
					<xsl:text>PRM-BW: </xsl:text>
					<xsl:choose>
						<xsl:when test="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent">
							<xsl:value-of select="ntb:replaceHTMLEntities(string(/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent), $containsCDATA)"/>
						</xsl:when>
						<xsl:when test="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='no']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent">
							<xsl:value-of select="ntb:replaceHTMLEntities(string(/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='no']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent), $containsCDATA)"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
				</xsl:choose>
					<!-- <xsl:value-of select="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent"/> -->
<!-- 						<xsl:value-of select="ntb:replaceHTMLEntities(string(/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent), $containsCDATA)"/>
-->
				</xsl:when>
				<xsl:when test="/NewsML/NewsItem/Identification/NewsIdentifier[ProviderId='omxgroup.com']">
					<xsl:text>PRM-OMX: </xsl:text>
          <xsl:choose>
            <xsl:when test="NewsComponent/NewsComponent[@xml:lang='no-NO']/Role[@FormalName='PRESSCONTENT']">
              <xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='no-NO']/NewsComponent/Role[@FormalName='Headline']"/>
            </xsl:when>
            <xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DK']/Role[@FormalName='PRESSCONTENT']">
              <xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DK']/NewsComponent/Role[@FormalName='Headline']"/>
            </xsl:when>
            <xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DA']/Role[@FormalName='PRESSCONTENT']">
              <xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DA']/NewsComponent/Role[@FormalName='Headline']"/>
            </xsl:when>
            <xsl:when test="NewsComponent/NewsComponent[@xml:lang='en-EN']/Role[@FormalName='PRESSCONTENT']">
              <xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='en-EN']/NewsComponent/Role[@FormalName='Headline']"/>
            </xsl:when>
            <xsl:when test="NewsComponent/NewsComponent[@xml:lang='fi-FI']/Role[@FormalName='PRESSCONTENT']">
              <xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/NewsComponent/Role[@FormalName='Headline']"/>
            </xsl:when>
            <xsl:when test="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']">
              <xsl:value-of select="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="NewsComponent/NewsLines/HeadLine"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="NewsComponent/NewsLines/HeadLine"/>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<meta name="timestamp">
				<xsl:attribute name="content"><xsl:value-of select="$meta-ts"/></xsl:attribute>
			</meta>
			<meta name="subject">
				<xsl:attribute name="content"><xsl:value-of select="$ntb-stikkord"/><!--<xsl:value-of select="Identification/NewsIdentifier/NewsItemID"/>
		<xsl:choose>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='no-NO']">
				<xsl:value-of select="NewsComponent/NewsComponent[@xml:lang='no-NO']/NewsLines/HeadLine"/>
			</xsl:when>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DK']">
				<xsl:value-of select="NewsComponent/NewsComponent[@xml:lang='da-DK']/NewsLines/HeadLine"/>
			</xsl:when>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='en-EN']">
				<xsl:value-of select="NewsComponent/NewsComponent[@xml:lang='en-EN']/NewsLines/HeadLine"/>
			</xsl:when>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='fi-FI']">
				<xsl:value-of select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/NewsLines/HeadLine"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NewsComponent/NewsLines/HeadLine"/>
			</xsl:otherwise>
		</xsl:choose>--></xsl:attribute>
			</meta>
			<!--<meta name="foldername">
	<xsl:attribute name="content">
	<xsl:text>Ut-Satellitt</xsl:text>
	</xsl:attribute>
	</meta>-->
			<meta name="filename">
				<xsl:attribute name="content"><xsl:value-of select="$filename"/></xsl:attribute>
			</meta>
			<meta name="NTBTjeneste" content="Formidlingstjenester"/>
      <xsl:if test="NewsComponent/AdministrativeMetadata/Creator/Party/Property[@FormalName='ForOrganisation']">
        <meta name="NTBKilde">
				  <xsl:attribute name="content"><xsl:value-of select="NewsComponent/AdministrativeMetadata/Creator/Party/Property[@FormalName='ForOrganisation']/@Value"/></xsl:attribute>
			  </meta>
      </xsl:if>
			<meta name="NTBMeldingsSign" content="prm"/>
			<meta name="NTBPrioritet">
				<xsl:attribute name="content"><!--<xsl:value-of select="/NewsML/NewsEnvelope/Priority/@FormalName"/>--><xsl:text>6</xsl:text></xsl:attribute>
			</meta>
			<meta name="NTBStikkord">
				<xsl:attribute name="content"><xsl:value-of select="$ntb-stikkord"/><!--<xsl:choose>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='no-NO']">
				<xsl:for-each select="NewsComponent/NewsComponent[@xml:lang='no-NO']/NewsLines/KeywordLine">
					<xsl:value-of select="."/>
					<xsl:text> </xsl:text>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DK']">
				<xsl:for-each select="NewsComponent/NewsComponent[@xml:lang='da-DK']/NewsLines/KeywordLine">
					<xsl:value-of select="."/>
					<xsl:text> </xsl:text>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='en-EN']">
				<xsl:for-each select="NewsComponent/NewsComponent[@xml:lang='en-EN']/NewsLines/KeywordLine">
					<xsl:value-of select="."/>
					<xsl:text> </xsl:text>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="NewsComponent/NewsComponent[@xml:lang='fi-FI']">
				<xsl:for-each select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/NewsLines/KeywordLine">
					<xsl:value-of select="."/>
					<xsl:text> </xsl:text>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>--></xsl:attribute>
			</meta>
			<meta name="NTBDistribusjonsKode" content="ALL"/>
			<meta name="NTBKanal" content="C"/>
			<meta name="NTBID">
				<xsl:attribute name="content"><xsl:value-of select="substring(Identification/NewsIdentifier/PublicIdentifier, string-length(Identification/NewsIdentifier/PublicIdentifier)-31)"/></xsl:attribute>
			</meta>
			<meta name="ntb-dato">
				<xsl:attribute name="content"><xsl:value-of select="$ntb-dato"/></xsl:attribute>
			</meta>
			<meta name="NTBIPTCSequence">
				<xsl:attribute name="content"><xsl:value-of select="$iptc_seq"/></xsl:attribute>
			</meta>
			<tobject>
				<xsl:attribute name="tobject.type">
          <xsl:choose>
            <xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']">
              <xsl:text>PRM-BWI</xsl:text>
            </xsl:when>
            <xsl:when test="/NewsML/NewsItem/Identification/NewsIdentifier[ProviderId='omxgroup.com']">
              <xsl:text>PRM-OMX</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>PRM-NTB</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
				<tobject.property tobject.property.type="Fulltekstmeldinger"/>
				<xsl:choose>
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='no-NO']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='no-NO']/DescriptiveMetadata/SubjectCode/Subject"/>
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='no-NO']/DescriptiveMetadata/SubjectCode/SubjectMatter"/>
					</xsl:when>
					
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DK']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DK']/DescriptiveMetadata/SubjectCode/Subject"/>
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DK']/DescriptiveMetadata/SubjectCode/SubjectMatter"/>
					</xsl:when>
					
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DA']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DA']/DescriptiveMetadata/SubjectCode/Subject"/>
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DA']/DescriptiveMetadata/SubjectCode/SubjectMatter"/>
					</xsl:when>
					
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='en-EN']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='en-EN']/DescriptiveMetadata/SubjectCode/Subject"/>
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='en-EN']/DescriptiveMetadata/SubjectCode/SubjectMatter"/>
					</xsl:when>
					
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='fi-FI']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/DescriptiveMetadata/SubjectCode/Subject"/>
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/DescriptiveMetadata/SubjectCode/SubjectMatter"/>
					</xsl:when>

					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='fi-FI']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/DescriptiveMetadata/SubjectCode/Subject"/>
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/DescriptiveMetadata/SubjectCode/SubjectMatter"/>
					</xsl:when>
				</xsl:choose>
			</tobject>
			<!--xsl:copy-of select="tobject"/-->
			<xsl:apply-templates select="NewsComponent"/>
			<!--<xsl:apply-templates select="NewsComponent/NewsComponent"/>-->
		</head>
		<body>
			<body.head>
				<hedline>
					<hl1>
				    <xsl:choose>
						<xsl:when test="/NewsML/NewsItem/Identification/NewsIdentifier[ProviderId='omxgroup.com']">
							<xsl:text>
    						OMX: 
		    				</xsl:text>
						</xsl:when>
						<xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']">
							<xsl:text>
    						BW: 
		    				</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>
    						PRM: 
		    				</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="NewsComponent/AdministrativeMetadata/Creator/Party/Property[@FormalName='ForOrganisation']">
							<xsl:value-of select="NewsComponent/AdministrativeMetadata/Creator/Party/Property[@FormalName='ForOrganisation']/@Value"/>
						</xsl:when>
						<xsl:when test="/NewsML/NewsItem/NewsComponent/AdministrativeMetadata/Source/Party/@FormalName">
							<xsl:value-of select="/NewsML/NewsItem/NewsComponent/AdministrativeMetadata/Source/Party/@FormalName"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="NewsComponent/AdministrativeMetadata/Creator/Party/Property[@FormalName='Organisation']/@Value"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>
					/
					</xsl:text>
						<xsl:choose>
							<xsl:when test="NewsComponent/NewsComponent[@xml:lang='no-NO']/Role[@FormalName='PRESSCONTENT']">
								<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='no-NO']/NewsComponent/Role[@FormalName='Headline']"/>
							</xsl:when>
							<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DK']/Role[@FormalName='PRESSCONTENT']">
								<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DK']/NewsComponent/Role[@FormalName='Headline']"/>
							</xsl:when>
							<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DA']/Role[@FormalName='PRESSCONTENT']">
								<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DA']/NewsComponent/Role[@FormalName='Headline']"/>
							</xsl:when>
							<xsl:when test="NewsComponent/NewsComponent[@xml:lang='en-EN']/Role[@FormalName='PRESSCONTENT']">
								<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='en-EN']/NewsComponent/Role[@FormalName='Headline']"/>
							</xsl:when>
							<xsl:when test="NewsComponent/NewsComponent[@xml:lang='fi-FI']/Role[@FormalName='PRESSCONTENT']">
								<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='fi-FI']/NewsComponent/Role[@FormalName='Headline']"/>
							</xsl:when>
							<xsl:when test="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']">
								<xsl:value-of select="ntb:replaceHTMLEntities(string(/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']), $containsCDATA)"/>
							</xsl:when>
							<xsl:when test="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='no']/NewsComponent[Role/@FormalName='HeadLine']">
								<xsl:value-of select="ntb:replaceHTMLEntities(string(/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='no']/NewsComponent[Role/@FormalName='HeadLine']/ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']), $containsCDATA)"/>
							</xsl:when>

						</xsl:choose>
					</hl1>
				</hedline>
				<distributor>
					<org>NTB</org>
				</distributor>
			</body.head>
			<body.content>
        <xsl:choose>
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='no-NO']/Role[@FormalName='PRESSCONTENT']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='no-NO']"/>
					</xsl:when>
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DK']/Role[@FormalName='PRESSCONTENT']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DK']"/>
					</xsl:when>
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='da-DA']/Role[@FormalName='PRESSCONTENT']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='da-DA']"/>
					</xsl:when>
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='en-EN']/Role[@FormalName='PRESSCONTENT']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='en-EN']"/>
					</xsl:when>
					<xsl:when test="NewsComponent/NewsComponent[@xml:lang='fi-FI']">
						<xsl:apply-templates select="NewsComponent/NewsComponent[@xml:lang='fi-FI']"/>
					</xsl:when>
					<xsl:when test="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='no']">
						<xsl:apply-templates select="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='no']"/>
					</xsl:when>
					<xsl:when test="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']">
						<xsl:apply-templates select="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']"/>
					</xsl:when>
          <xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
				<p class="txt-ind">
					<br/>
					<xsl:text>**** Dette stoff formidler NTB for andre - se www.ntb.no ****</xsl:text>
					<br/>
				</p>
			</body.content>
		</body>
	</xsl:template>
	<xsl:template match="NewsComponent">
		<docdata>
			<xsl:copy-of select="doc-id"/>
			<doc-id>
				<xsl:attribute name="reg-src"><xsl:value-of select="/NewsML/NewsItem/Identification/NewsIdentifier/ProviderId"/></xsl:attribute>
				<xsl:attribute name="id-string"><xsl:value-of select="substring(/NewsML/NewsItem/Identification/NewsIdentifier/PublicIdentifier, string-length(/NewsML/NewsItem/Identification/NewsIdentifier/PublicIdentifier)-31)"/></xsl:attribute>
			</doc-id>
			<urgency>
				<xsl:attribute name="ed-urg"><!--<xsl:value-of select="/NewsML/NewsItem/NewsManagement/Urgency/@FormalName"/>--><xsl:text>6</xsl:text></xsl:attribute>
			</urgency>
			<date.issue>
				<xsl:attribute name="norm"><xsl:value-of select="$date-issue.norm"/></xsl:attribute>
			</date.issue>
			<ed-msg>
				<xsl:attribute name="info">
					<xsl:choose>
						<xsl:when test="NewsComponent[@xml:lang='no-NO']/NewsComponent/Role[@FormalName='EditorialNote']">
							  <xsl:apply-templates select="NewsComponent[@xml:lang='no-NO']/NewsComponent/Role[@FormalName='EditorialNote']"/>
						</xsl:when>
						<xsl:when test="NewsComponent[@xml:lang='da-DK']/NewsComponent/Role[@FormalName='EditorialNote']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='da-DK']/NewsComponent/Role[@FormalName='EditorialNote']"/>
						</xsl:when>
						<xsl:when test="NewsComponent[@xml:lang='da-DA']/NewsComponent/Role[@FormalName='EditorialNote']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='da-DA']/NewsComponent/Role[@FormalName='EditorialNote']"/>
						</xsl:when>
						<xsl:when test="NewsComponent[@xml:lang='en-EN']/NewsComponent/Role[@FormalName='EditorialNote']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='en-EN']/NewsComponent/Role[@FormalName='EditorialNote']"/>
						</xsl:when>
						<xsl:when test="NewsComponent[@xml:lang='fi-FI']/NewsComponent/Role[@FormalName='EditorialNote']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='fi-FI']/NewsComponent/Role[@FormalName='EditorialNote']"/>
						</xsl:when>
						<xsl:otherwise><xsl:text>**** NTBs PRESSEMELDINGSTJENESTE - Se www.ntb.no ****</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</ed-msg>
			<du-key>
				<xsl:attribute name="version"><xsl:value-of select="/NewsML/NewsItem/Identification/NewsIdentifier/RevisionId"/></xsl:attribute>
				<xsl:attribute name="key"><xsl:value-of select="$ntb-stikkord"/></xsl:attribute>
			</du-key>
			<doc.copyright>
				<xsl:attribute name="year"><xsl:value-of select="substring($timestamp, 1, 4)"/></xsl:attribute>
				<xsl:attribute name="holder">
					<xsl:choose>
						<xsl:when test="AdministrativeMetadata/Creator/Party/Property[@FormalName='Organisation']/@Value">
							<xsl:value-of select="AdministrativeMetadata/Creator/Party/Property[@FormalName='Organisation']/@Value"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="AdministrativeMetadata/Source/Party/@FormalName"/>
						</xsl:otherwise>
					</xsl:choose>				
				</xsl:attribute>
			</doc.copyright>
			<key-list>
				<keyword>
					<xsl:attribute name="key"><xsl:value-of select="$ntb-stikkord"/><!--<xsl:choose>
						<xsl:when test="NewsComponent[@xml:lang='no-NO']/Role[@FormalName='PRESSCONTENT']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='no-NO']/NewsLines/KeywordLine"/>
						</xsl:when>
						<xsl:when test="NewsComponent[@xml:lang='da-DK']/Role[@FormalName='PRESSCONTENT']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='da-DK']/NewsLines/KeywordLine"/>
						</xsl:when>
						<xsl:when test="NewsComponent[@xml:lang='en-EN']/Role[@FormalName='PRESSCONTENT']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='en-EN']/NewsLines/KeywordLine"/>
						</xsl:when>
						<xsl:when test="NewsComponent[@xml:lang='fi-FI']/Role[@FormalName='PRESSCONTENT']">
							<xsl:apply-templates select="NewsComponent[@xml:lang='fi-FI']/NewsLines/KeywordLine"/>
						</xsl:when>
					</xsl:choose>--></xsl:attribute>
				</keyword>
			</key-list>
		</docdata>
		<pubdata>
			<xsl:attribute name="date.publication"><xsl:value-of select="/NewsML/NewsEnvelope/DateAndTime"/></xsl:attribute>
			<xsl:attribute name="item-lenght">
		</xsl:attribute>
			<xsl:attribute name="unit-of-measure"><xsl:text>character</xsl:text></xsl:attribute>
		</pubdata>
		<revision-history>
			<xsl:attribute name="name"><xsl:text>prm</xsl:text></xsl:attribute>
		</revision-history>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='no-NO']">
		<xsl:if test="Role[@FormalName='PRESSCONTENT']">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Short']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Body']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Contact']"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='PICTURE']">
			<xsl:call-template name="PICTURE"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='LOGO']">
			<xsl:call-template name="LOGO"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='BINARY']">
			<xsl:call-template name="BINARY"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='URL']">
			<xsl:call-template name="URL"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DK']">
		<xsl:if test="Role[@FormalName='PRESSCONTENT']">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Short']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Body']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Contact']"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='PICTURE']">
			<xsl:call-template name="PICTURE"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='LOGO']">
			<xsl:call-template name="LOGO"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='BINARY']">
			<xsl:call-template name="BINARY"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='URL']">
			<xsl:call-template name="URL"/>
		</xsl:if>
	</xsl:template>
  <xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DA']">
		<xsl:if test="Role[@FormalName='PRESSCONTENT']">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Short']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Body']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Contact']"/>
		</xsl:if>
	    <xsl:if test="Role[@FormalName='PICTURE']">
			<xsl:call-template name="PICTURE"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='LOGO']">
			<xsl:call-template name="LOGO"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='BINARY']">
			<xsl:call-template name="BINARY"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='URL']">
			<xsl:call-template name="URL"/>
		</xsl:if>
  </xsl:template>
  <xsl:template match="NewsComponent/NewsComponent[@xml:lang='en-EN']">
		<xsl:if test="Role[@FormalName='PRESSCONTENT']">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Short']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Body']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Contact']"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='PICTURE']">
			<xsl:call-template name="PICTURE"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='LOGO']">
			<xsl:call-template name="LOGO"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='BINARY']">
			<xsl:call-template name="BINARY"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='URL']">
			<xsl:call-template name="URL"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='fi-FI']">
		<xsl:if test="Role[@FormalName='PRESSCONTENT']">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Short']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Body']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Contact']"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='PICTURE']">
			<xsl:call-template name="PICTURE"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='LOGO']">
			<xsl:call-template name="LOGO"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='BINARY']">
			<xsl:call-template name="BINARY"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='URL']">
			<xsl:call-template name="URL"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en' or DescriptiveMetadata/Language/@FormalName='no']">
		<xsl:apply-templates select="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']"/>
		<xsl:apply-templates select="NewsComponent/Role[@FormalName='Body']"/>
		<xsl:apply-templates select="NewsComponent/Role[@FormalName='Contact']"/>
		<xsl:if test="Role[@FormalName='PICTURE']">
			<xsl:call-template name="PICTURE"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='LOGO']">
			<xsl:call-template name="LOGO"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='BINARY']">
			<xsl:call-template name="BINARY"/>
		</xsl:if>
		<xsl:if test="Role[@FormalName='URL']">
			<xsl:call-template name="URL"/>
		</xsl:if>
	</xsl:template>
  <xsl:template match="NewsComponent/NewsComponent[@xml:lang='no-NO']/NewsComponent/Role[@FormalName='Headline']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DK']/NewsComponent/Role[@FormalName='Headline']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DA']/NewsComponent/Role[@FormalName='Headline']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='en-EN']/NewsComponent/Role[@FormalName='Headline']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='fi-FI']/NewsComponent/Role[@FormalName='Headline']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<!--<xsl:template match="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent/Role[@FormalName='Body']">
		<xsl:value-of select="../ContentItem/Datacontent"/>"
	</xsl:template>-->
	<xsl:template match="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']">
    <hl2>
      <xsl:value-of select="ntb:replaceHTMLEntities(string(/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en' or DescriptiveMetadata/Language/@FormalName='no']/NewsComponent[Role/@FormalName='SubHeadLine']/ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p']), $containsCDATA)"/>
    </hl2>
    <p>
			<xsl:attribute name="lede"><xsl:text>true</xsl:text></xsl:attribute>
			<xsl:attribute name="class"><xsl:text>lead</xsl:text></xsl:attribute>
			<xsl:value-of select="ntb:replaceHTMLEntities(string(/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en' or DescriptiveMetadata/Language/@FormalName='no']/NewsComponent[Role/@FormalName='Body']/ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p' and position()=1]), $containsCDATA)"/>
			<!-- <xsl:value-of select="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en' or DescriptiveMetadata/Language/@FormalName='no']/NewsComponent[Role/@FormalName='Body']/ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p' and position()=1]"/> -->
			<!--<xsl:value-of select="/NewsML/NewsItem/NewsComponent/NewsComponent[DescriptiveMetadata/Language/@FormalName='en']/NewsComponent[Role/@FormalName='Contact']/ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p']"/>-->
		</p>
	</xsl:template>
	<xsl:template match="NewsComponent/Role[@FormalName='Short']">
		<p>
			<xsl:attribute name="lede"><xsl:text>true</xsl:text></xsl:attribute>
			<xsl:attribute name="class"><xsl:text>lead</xsl:text></xsl:attribute>
			<xsl:value-of select="../ContentItem/DataContent"/>
		</p>
	</xsl:template>
	<xsl:template match="NewsComponent/Role[@FormalName='Body']">
    <!--<xsl:apply-templates select="../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']">
      <xsl:with-param name="dropLead">
        <xsl:copy-of select="true()"/>
      </xsl:with-param>
    </xsl:apply-templates>-->
    <xsl:variable name="bodywithHTML">
			<xsl:choose>
				<xsl:when test="function-available('ntb:replaceHTMLEntities')">
					<xsl:choose>
						<xsl:when test="not(/NewsML/NewsEnvelope/SentFrom) or /NewsML/NewsEnvelope/SentFrom/Party[@FormalName!='Business Wire']">
							<xsl:value-of select="ntb:replaceHTMLEntities(string(../ContentItem/DataContent), $containsCDATA)" disable-output-escaping="yes"/>
						</xsl:when>
						<xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire'] and ../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p' and position()>1]">
              <xsl:apply-templates select="../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']">
                <xsl:with-param name="dropLead">
                  <xsl:text>true</xsl:text>
                </xsl:with-param>
              </xsl:apply-templates>
							<!-- <xsl:for-each select="../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p' and position()>1]">
								<p>
									<xsl:value-of select="concat(concat('&lt;p&gt;', ntb:replaceHTMLEntities(string(.), $containsCDATA)), '&lt;/p&gt;')" disable-output-escaping="yes"/>
								</p>
							</xsl:for-each>	-->
						</xsl:when>
					</xsl:choose>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="not(/NewsML/NewsEnvelope/SentFrom) or /NewsML/NewsEnvelope/SentFrom/Party[@FormalName!='Business Wire']">
				<p>
					<xsl:value-of select="$bodywithHTML" disable-output-escaping="yes"/>
				</p>
			</xsl:when>
			<xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']">
				<xsl:value-of select="$bodywithHTML" disable-output-escaping="yes"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="NewsComponent/Role[@FormalName='Contact']">
    <!--<xsl:apply-templates select="../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']">
      <xsl:with-param name="dropLead">
        <xsl:copy-of select="false()"/>
      </xsl:with-param>
    </xsl:apply-templates>-->
    <xsl:variable name="contactwithHTML">
			<xsl:choose>
				<xsl:when test="function-available('ntb:replaceHTMLEntities')">
					<xsl:choose>
						<xsl:when test="not(/NewsML/NewsEnvelope/SentFrom) or /NewsML/NewsEnvelope/SentFrom/Party[@FormalName!='Business Wire']">
							<xsl:value-of select="ntb:replaceHTMLEntities(string(../ContentItem/DataContent), $containsCDATA)" disable-output-escaping="yes"/>
						</xsl:when>
			
						<xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire'] and ../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p']">
							<xsl:apply-templates select="../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']">
								<xsl:with-param name="dropLead">
								<xsl:text>false</xsl:text>
								</xsl:with-param>
							</xsl:apply-templates>
							
							<!-- <xsl:for-each select="../ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']/*[local-name()='p' and position()>1]">
								<p>
									<xsl:value-of select="concat(concat('&lt;p&gt;', ntb:replaceHTMLEntities(string(.), $containsCDATA)), '&lt;/p&gt;')" disable-output-escaping="yes"/>
								</p>
							</xsl:for-each>	-->
						</xsl:when>
					</xsl:choose>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="not(/NewsML/NewsEnvelope/SentFrom) or /NewsML/NewsEnvelope/SentFrom/Party[@FormalName!='Business Wire']">
				<p>
					<xsl:value-of select="$contactwithHTML" disable-output-escaping="yes"/>
				</p>
			</xsl:when>
			<xsl:when test="/NewsML/NewsEnvelope/SentFrom/Party[@FormalName='Business Wire']">
				<xsl:value-of select="$contactwithHTML" disable-output-escaping="yes"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="PICTURE">
		<media media-type="image" class="prs">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='HighRes']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Caption']"/>
		</media>
	</xsl:template>
	<xsl:template name="LOGO">
		<media media-type="image" class="prs">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='HighRes']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Hyperlink']"/>
		</media>
	</xsl:template>
	<xsl:template name="BINARY">
		<media media-type="document" class="prs">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Supplementary']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Caption']"/>
		</media>
	</xsl:template>
	<xsl:template name="URL">
		<media media-type="url" class="prs">
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Hyperlink']"/>
			<xsl:apply-templates select="NewsComponent/Role[@FormalName='Caption']"/>
		</media>
	</xsl:template>
	<xsl:template match="NewsComponent/Role[@FormalName='HighRes']">
		<media-reference>
			<xsl:attribute name="name"><xsl:value-of select="../ContentItem/Characteristics/Property[@FormalName='FileName']/@Value"/></xsl:attribute>
			<xsl:attribute name="mime-type"><xsl:text>image/</xsl:text><xsl:value-of select="../ContentItem/Format/@FormalName"/></xsl:attribute>
			<xsl:attribute name="alternate-text"><xsl:value-of select="$URLToImages"/></xsl:attribute>
			<xsl:attribute name="source"><xsl:value-of select="../ContentItem/@Href"/></xsl:attribute>
		</media-reference>
	</xsl:template>
	<xsl:template match="NewsComponent/Role[@FormalName='Hyperlink']">
		<media-caption>
			<xsl:call-template name="replaceHTMLEntities">
				<xsl:with-param name="stringToProcess">
					<xsl:value-of select="string('&lt;a HRef=&quot;')" disable-output-escaping="yes"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:value-of select="../ContentItem/@Href"/>
			<xsl:call-template name="replaceHTMLEntities">
				<xsl:with-param name="stringToProcess">
					<xsl:value-of select="string('&quot; target=&quot;_blank&quot;&gt;')" disable-output-escaping="yes"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:value-of select="../ContentItem/@Href"/>
			<xsl:call-template name="replaceHTMLEntities">
				<xsl:with-param name="stringToProcess">
					<xsl:value-of select="string('&lt;/a&gt;')" disable-output-escaping="yes"/>
				</xsl:with-param>
			</xsl:call-template>
		</media-caption>
	</xsl:template>
	<xsl:template match="NewsComponent/Role[@FormalName='Supplementary']">
		<media-reference>
			<xsl:attribute name="name"><xsl:value-of select="../ContentItem/Characteristics/Property[@FormalName='FileName']/@Value"/></xsl:attribute>
			<xsl:attribute name="mime-type"><xsl:value-of select="../ContentItem/MediaType/@FormalName"/><xsl:text>/</xsl:text><xsl:value-of select="../ContentItem/Format/@FormalName"/></xsl:attribute>
			<xsl:attribute name="alternate-text"><xsl:value-of select="$URLToAttachments"/></xsl:attribute>
			<xsl:attribute name="source"><xsl:value-of select="../ContentItem/@Href"/></xsl:attribute>
		</media-reference>
	</xsl:template>
	<xsl:template match="NewsComponent/Role[@FormalName='Caption']">
		<media-caption>
			<xsl:value-of select="../ContentItem/DataContent"/>
		</media-caption>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='no-NO']/NewsComponent/Role[@FormalName='EditorialNote']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='da-DK']/NewsComponent/Role[@FormalName='EditorialNote']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='da-DA']/NewsComponent/Role[@FormalName='EditorialNote']">
	    <xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='en-EN']/NewsComponent/Role[@FormalName='EditorialNote']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='fi-FI']/NewsComponent/Role[@FormalName='EditorialNote']">
		<xsl:value-of select="../ContentItem/DataContent"/>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='no-NO']/NewsLines/KeywordLine">
		<xsl:value-of select="."/>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='da-DK']/NewsLines/KeywordLine">
		<xsl:value-of select="."/>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='da-DA']/NewsLines/KeywordLine">
		<xsl:value-of select="."/>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='en-EN']/NewsLines/KeywordLine">
		<xsl:value-of select="."/>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="NewsComponent[@xml:lang='fi-FI']/NewsLines/KeywordLine">
		<xsl:value-of select="."/>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='no-NO']/DescriptiveMetadata/SubjectCode/Subject">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.type"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='no-NO']/DescriptiveMetadata/SubjectCode/SubjectMatter">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.matter"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DK']/DescriptiveMetadata/SubjectCode/Subject">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.type"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DK']/DescriptiveMetadata/SubjectCode/SubjectMatter">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.matter"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
  <xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DA']/DescriptiveMetadata/SubjectCode/Subject">
    <tobject.subject>
      <xsl:attribute name="tobject.subject.code">
        <xsl:choose>
          <xsl:when test="function-available('ntb:getSubjectNameShort')">
            <xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="tobject.subject.refnum">
        <xsl:value-of select="@FormalName"/>
      </xsl:attribute>
      <xsl:attribute name="tobject.subject.type">
        <xsl:choose>
          <xsl:when test="function-available('ntb:getSubjectType')">
            <xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
    </tobject.subject>
  </xsl:template>
  <xsl:template match="NewsComponent/NewsComponent[@xml:lang='da-DA']/DescriptiveMetadata/SubjectCode/SubjectMatter">
    <tobject.subject>
      <xsl:attribute name="tobject.subject.code">
        <xsl:choose>
          <xsl:when test="function-available('ntb:getSubjectNameShort')">
            <xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="tobject.subject.refnum">
        <xsl:value-of select="@FormalName"/>
      </xsl:attribute>
      <xsl:attribute name="tobject.subject.matter">
        <xsl:choose>
          <xsl:when test="function-available('ntb:getSubjectType')">
            <xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
    </tobject.subject>
  </xsl:template>
  <xsl:template match="NewsComponent/NewsComponent[@xml:lang='en-EN']/DescriptiveMetadata/SubjectCode/Subject">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.type"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='en-EN']/DescriptiveMetadata/SubjectCode/SubjectMatter">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.matter"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='fi-FI']/DescriptiveMetadata/SubjectCode/Subject">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.type"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
	<xsl:template match="NewsComponent/NewsComponent[@xml:lang='fi-FI']/DescriptiveMetadata/SubjectCode/SubjectMatter">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code"><xsl:choose><xsl:when test="function-available('ntb:getSubjectNameShort')"><xsl:value-of select="ntb:getSubjectNameShort(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@FormalName"/></xsl:attribute>
			<xsl:attribute name="tobject.subject.matter"><xsl:choose><xsl:when test="function-available('ntb:getSubjectType')"><xsl:value-of select="ntb:getSubjectType(number(@FormalName))"/></xsl:when></xsl:choose></xsl:attribute>
		</tobject.subject>
	</xsl:template>
	<xsl:template name="replace-html">
		<xsl:element name="{local-name(.)}">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*">
		<xsl:attribute name="{local-name(.)}"><xsl:value-of select="." disable-output-escaping="yes"/></xsl:attribute>
	</xsl:template>
	<xsl:template name="replaceHTMLEntities">
		<xsl:param name="stringToProcess"/>
		<xsl:choose>
			<xsl:when test="function-available('ntb:replaceHTMLEntities')">
				<xsl:value-of select="ntb:replaceHTMLEntities(string($stringToProcess), $containsCDATA)" disable-output-escaping="yes"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<!-- Returns position of first instance of substring found in argument string -->
	<xsl:template name="index-of-string-first">
	<xsl:param name="arg"/>
	<xsl:param name="substring"/>
	<xsl:if test="contains($arg, $substring)">
	<xsl:value-of select="string-length(substring-before($arg, $substring))+1"/>
	</xsl:if>
	</xsl:template>
	
	<xsl:template match="ContentItem/DataContent/*[local-name()='html' and namespace-uri()='http://www.w3.org/1999/xhtml']/*[local-name()='body']" >
    <xsl:param name="dropLead"/>
		<xsl:variable name="xhtmlBody">
      <xsl:apply-templates select="@*|node()">
        <xsl:with-param name="dropLead">
          <xsl:copy-of select="$dropLead"/>
        </xsl:with-param>
      </xsl:apply-templates>
		</xsl:variable>
    <xsl:choose>
      <xsl:when test="function-available('ntb:replaceHTMLEntities')">
        <xsl:copy-of select="ntb:replaceHTMLEntities($xhtmlBody, $containsCDATA)" />
        <!-- With the use of XSLT version 2 this could have been helpful for change bold-tags into hl2-tags: -->
        <!-- replace(ntb:replaceHTMLEntities($xhtmlBody, $containsCDATA), '&lt;b&gt;','&lt;hl2&gt;', 'i') -->
      </xsl:when>
      <xsl:when test="not($dropLead=true())">
        <!-- <xsl:copy-of select="$xhtmlBody"/>-->
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:template>

	<!-- the identity template -->
	<xsl:template match="@*|node()">
    <xsl:param name="dropLead"/>
	  <xsl:copy>
      <!-- ../../../../Role[@FormalName='Contact'] and -->
      <xsl:choose>
        <xsl:when test="$dropLead='true'">
          <xsl:if test="not (local-name()='p' and position()=1)" >
            <xsl:apply-templates select="@*|node()"/>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:otherwise>
      </xsl:choose>
	  </xsl:copy>
	</xsl:template>
</xsl:stylesheet>
