<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="ISO-8859-1" indent="yes"/>

<xsl:param name="filename">
</xsl:param>

<xsl:param name="iptc_seq">
</xsl:param>

<xsl:variable name="timestamp" select="concat(/EquityStory-News/header/deliveryDate, /EquityStory-News/header/deliveryTime)" />

<xsl:variable name="date-issue.norm">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 11, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 14, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 17, 2)" />
</xsl:variable>

<xsl:variable name="meta-ts">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text> </xsl:text>
	<xsl:value-of select="substring($timestamp, 11, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 14, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 17, 2)" />
</xsl:variable>

<xsl:variable name="ntb-dato">
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text> </xsl:text>
	<xsl:value-of select="substring($timestamp, 11, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 14, 2)" />
</xsl:variable>

<xsl:variable name="xml-timestamp">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
	<xsl:value-of select="substring($timestamp, 6, 2)" />
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 11, 2)" />
	<xsl:value-of select="substring($timestamp, 14, 2)" />
	<xsl:value-of select="substring($timestamp, 17, 2)" />
</xsl:variable>

<xsl:template match="/">
	<xsl:apply-templates select="EquityStory-News"/>
</xsl:template>

<xsl:template match="EquityStory-News">
	<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
	<head>
		<title><xsl:value-of select="issuer/NewsItem/Headline"/></title>

		<meta name="timestamp">
			<xsl:attribute name="content">
				<xsl:value-of select="$meta-ts"/>
			</xsl:attribute>
		</meta>
		<meta name="foldername">
			<xsl:attribute name="content">
				<xsl:text>Ut-Satellitt</xsl:text>
			</xsl:attribute>
		</meta>
		<meta name="filename">
			<xsl:attribute name="content">
				<xsl:value-of select="$filename"/>
			</xsl:attribute>
		</meta>
		<meta name="NTBTjeneste" content="Formidlingstjenester" />
		<meta name="NTBMeldingsSign" content="EquityStory" />
		<meta name="NTBPrioritet" content="6" />
		<meta name="NTBStikkord" content="" />
		<meta name="NTBDistribusjonsKode" content="ALL" />
		<meta name="NTBKanal" content="C" />
		<meta name="NTBIPTCSequence">
			<xsl:attribute name="content">
				<xsl:value-of select="$iptc_seq" />
			</xsl:attribute>
		</meta>
		<meta name="NTBID">
			<xsl:attribute name="content">
				<xsl:text>EquityStory:NTB:</xsl:text>
				<xsl:value-of select="header/interneId"/>
			</xsl:attribute>
		</meta>
		<meta name="NTBUtDato">
			<xsl:attribute name="content">
				<xsl:value-of select="$ntb-dato" />
			</xsl:attribute>
		</meta>
		<meta name="ntb-dato">
			<xsl:attribute name="content">
				<xsl:value-of select="$ntb-dato" />
			</xsl:attribute>
		</meta>
		
		<tobject tobject.type="PRM-EquityStory">
			<tobject.property tobject.property.type="Fulltekstmeldinger"/>
		</tobject>
		
		<docdata>
			<doc-id>
				<xsl:attribute name="regsrc">NTB</xsl:attribute>
				<xsl:attribute name="id-string">
					<xsl:text>EquityStory:NTB:</xsl:text>
					<xsl:value-of select="header/interneId"/>
				</xsl:attribute>
			</doc-id>
			
			<urgency ed-urg="6" />
			
			<date.issue>
				<xsl:attribute name="norm">
				<xsl:value-of select="$date-issue.norm"/>
				</xsl:attribute>
			</date.issue>
			<ed-msg>
				<xsl:attribute name="info">
				</xsl:attribute>
			</ed-msg>
			
			<du-key>
				<xsl:attribute name="version">1</xsl:attribute>
				<xsl:attribute name="key">
					<xsl:value-of select="issuer/NewsItem/Headline"/>
				</xsl:attribute>
			</du-key>

			<doc.copyright>
				<xsl:attribute name="year"><xsl:value-of select="substring(header/deliveryDate, 1,4)" /></xsl:attribute>
				<xsl:attribute name="holder"><xsl:value-of select="header/senderName"/></xsl:attribute>
			</doc.copyright>
			
			<key-list>
				<keyword>
					<xsl:attribute name="key">
						<xsl:value-of select="issuer/NewsItem/Headline"/>
					</xsl:attribute>
				</keyword>
			</key-list>
		</docdata>
		<pubdata>
			<xsl:attribute name="date.publication"><xsl:value-of select="$xml-timestamp" /></xsl:attribute>
			<xsl:attribute name="item-length"><xsl:value-of select="string-length(/issuer/NewsItem/Story)"/></xsl:attribute>
			<xsl:attribute name="unit-of-measure">characters</xsl:attribute>
		</pubdata>
		<revision-history name="EquityStory" />
	</head>
	<body>
		<body.head>
			<hedline>
				<hl1><xsl:value-of select="issuer/NewsItem/Headline"/></hl1>
			</hedline>
			<distributor><org>NTB</org></distributor>
		</body.head>
		<body.content>
			<p class="txt">
				<xsl:value-of select="issuer/NewsItem/Story" />
			</p>
			<p class="txt">
				<xsl:value-of select="header/senderName" />
				<xsl:text> DGAP-</xsl:text>
				<xsl:value-of select="issuer/NewsItem/news_type" />
			</p>
			<p class="txt">
				<xsl:value-of select="header/deliveryDate" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="header/deliveryTime" />
			</p>
			<p class="txt">
				<xsl:value-of select="issuer/issuerIdentification/companyName" />
				<xsl:text>, </xsl:text>
				<xsl:value-of select="issuer/NewsItem/country" />
			</p>
			<p class="txt">
			<xsl:text>**** Dette stoff formidler NTB for andre - se www.ntb.no ****</xsl:text>
			</p>
		</body.content>
	</body>
	</nitf>
</xsl:template>
</xsl:stylesheet>