Imports ntb_FuncLib
Imports System.io
Imports System.Text
Imports System.Text.RegularExpressions

Public Class BWConverter

    Public Shared Function ConvertBWFile2NITF(ByRef f As String, ByVal iptc As Integer) As String

        'Content
        Dim content As String = LogFile.ReadFile(f)
        Dim timestamp As DateTime = File.GetLastWriteTime(f)
        f = "BWI-" & timestamp.ToString("yyyyMMdd-HHmmss")

        Dim pgps As String()
        pgps = Regex.Split(content, vbCrLf & vbCrLf)

        'Use Regex to extract and separate keywords an title
        Dim pat As String = "^\s*(\(.+?\))+"
        Dim m As Match = Regex.Match(pgps(0), pat)
        Dim so As String = m.Value
        Dim title As String = pgps(0).Substring(m.Index + m.Length)

        title = "PRM: BWI-" & MakeXMLElement(title)
        so = MakeXMLElement(so)
        so = Regex.Replace(so, "\)\(", "-")
        so = Regex.Replace(so, "[\)\( ]+", "")
        so = "PRM: " & so

        Dim header As String = MakeNITF32Header(f, timestamp, title, so, content.Length, iptc, "BWI", "bw")
        Dim output As StringBuilder = New StringBuilder

        output.Append("<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & vbCrLf)

        output.Append("<!--<!DOCTYPE nitf SYSTEM ""nitf-3-2.dtd"">-->" & vbCrLf)
        output.Append("<nitf version=""-//IPTC//DTD NITF 3.2//EN"" change.date=""October 10, 2003"" change.time=""19:30"" baselang=""no-NO"">" & vbCrLf)

        output.Append(header)

        output.Append("<body>" & vbCrLf)

        output.Append("<body.head>" & vbCrLf)
        output.Append("<hedline><hl1>" & title & "</hl1></hedline>" & vbCrLf)
        output.Append("<distributor><org>NTB</org></distributor>" & vbCrLf)
        output.Append("</body.head>" & vbCrLf)

        output.Append("<body.content>" & vbCrLf)

        output.Append("<p lede=""true"">" & title & "</p>" & vbCrLf)

        Dim i As Integer
        For i = 1 To pgps.GetUpperBound(0)
            output.Append("<p>" & MakeXMLElement(pgps(i)) & "</p>" & vbCrLf)
        Next

        output.Append("</body.content>" & vbCrLf)

        output.Append("</body>" & vbCrLf)
        output.Append("</nitf>" & vbCrLf)

        Return output.ToString
    End Function

    Private Shared Function MakeNITF32Header(ByVal fn As String, ByVal dt As DateTime, ByVal title As String, ByVal so As String, ByVal length As Integer, ByVal iptc As Integer, ByVal msgSource As String, ByVal msgSign As String) As String

        Dim output As StringBuilder = New StringBuilder

        Dim id As String = msgSource & ":NTB:" & dt.ToString("yyyyMMdd:mmss:") & length Mod 1000

        output.Append("<head>" & vbCrLf)
        output.Append("<title>" & title & "</title>" & vbCrLf)

        output.Append("<meta name=""timestamp"" content=""" & dt.ToString("yyyy.MM.dd HH:mm:ss") & """ />" & vbCrLf)

        output.Append("<meta name=""subject"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""foldername"" content=""Ut-Satellitt"" />" & vbCrLf)
        output.Append("<meta name=""filename"" content=""" & fn & ".xml"" />" & vbCrLf)

        output.Append("<meta name=""NTBTjeneste"" content=""Formidlingstjenester"" />" & vbCrLf)
        output.Append("<meta name=""NTBMeldingsSign"" content=""" & msgSign & "/"" />" & vbCrLf)
        output.Append("<meta name=""NTBPrioritet"" content=""6"" />" & vbCrLf)
        output.Append("<meta name=""NTBStikkord"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""NTBDistribusjonsKode"" content=""ALL"" />" & vbCrLf)
        output.Append("<meta name=""NTBKanal"" content=""C"" />" & vbCrLf)
        output.Append("<meta name=""NTBIPTCSequence"" content=""" & iptc & """ />" & vbCrLf)
        output.Append("<meta name=""NTBID"" content=""" & id & """ />" & vbCrLf)

        output.Append("<meta name=""ntb-dato"" content=""" & dt.ToString("dd.MM.yyyy HH:mm") & """ />" & vbCrLf)

        output.Append("<tobject tobject.type=""PRM-" & msgSource & """>" & vbCrLf)
        output.Append("<tobject.property tobject.property.type=""Fulltekstmeldinger"" />" & vbCrLf)
        output.Append("</tobject>" & vbCrLf)

        output.Append("<docdata>" & vbCrLf)
        output.Append("<doc-id regsrc=""NTB"" id-string=""" & id & """/>" & vbCrLf)
        output.Append("<urgency ed-urg=""6""/>" & vbCrLf)
        output.Append("<date.issue norm=""" & dt.ToString("yyyy-MM-ddTHH:mm:ss") & """/>" & vbCrLf)
        output.Append("<ed-msg info="""" />" & vbCrLf)
        output.Append("<du-key version=""1"" key=""" & so & """ />" & vbCrLf)
        output.Append("<doc.copyright year=""" & dt.ToString("yyyy") & """ holder=""NTB"" /> " & vbCrLf)
        output.Append("<key-list>" & vbCrLf)
        output.Append("<keyword key=""" & so & """ />" & vbCrLf)
        output.Append("</key-list>" & vbCrLf)
        output.Append("</docdata>" & vbCrLf)

        output.Append("<pubdata date.publication=""" & dt.ToString("yyyyMMddTHHmmss") & """ item-length=""" & length & """ unit-of-measure=""character"" />" & vbCrLf)

        output.Append("<revision-history name=""" & msgSign & "/"" />" & vbCrLf)

        output.Append("</head>" & vbCrLf)

        Return output.ToString()

    End Function

    Private Shared Function MakeXMLElement(ByVal inn As String) As String

        Dim out As String = inn.Trim()
        out = Regex.Replace(out, " +", " ")
        out = Regex.Replace(out, "[" & Chr(10) & Chr(13) & "]+", " ")

        out = out.Replace("&", "&amp;")
        out = out.Replace("<", "&lt;")
        out = out.Replace(">", "&gt;")
        out = out.Replace("""", "&quot;")

        Return out
    End Function
End Class
