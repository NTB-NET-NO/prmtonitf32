Imports ntb_FuncLib
Imports System.io
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Array
Public Class OCDFConverter

    Public Sub New()

    End Sub

    Function ConvertOCDF2NITF(ByRef file As String, ByVal iptc As Integer) As String
        Dim OCDFContent As String = ""
        Dim sErrInfo As String = ""
        OCDFContent = GetFileContents(file, iptc, sErrInfo)
        Return OCDFContent
    End Function
    Class OCDFMessage
        Public newsSource, newsTimestamp, newsUrl, newsLanguage, newsMessageId, newsType, newsTypeEnglish, compId, isinCode, orderBookId, newsHeader, newsLength, newsMsg, newsCorrectionId As String

        Public Sub New()

        End Sub

    End Class
    Class OCDFFixedDataIssuers
        Public compId, compName, earningsPrShare, lastFiscalYearEnd, retOnEqty As String

        Public Sub New()

        End Sub

    End Class
    Class OCDFFixedDataEquity
        Public adjFact, adjFactExDate, compId, currencyDenomination, equityClass, isinCode, issueDate, listingCategory, listingClass, marketCode, maturityDate, nextDividendAdj, nextDividendExDate, nomValue, outstandingNoOfShares, secClass, secName, secSector, symbol, totalNoOfShares, tradable As String

        Public Sub New()

        End Sub

    End Class

    Function GetFileContents(ByRef FullPath As String, ByVal iptc As Integer, Optional ByRef ErrInfo As String = "") As String
        GetFileContents = ""
        Dim fileContents As String = ""
        Dim strConverted As String = ""
        Dim strOCDFBit As String = ""
        Dim objReader As StreamReader = Nothing
        Try
            Dim i As Integer
            Dim a() As String
            Dim Seps(1) As Object

            Seps(0) = ";"
            Dim intIssuers As Integer = -1
            Dim ocdfIssuers() As OCDFFixedDataIssuers = Nothing
            Try
                objReader = New StreamReader(FullPath.Substring(0, FullPath.LastIndexOf("\")) & "\FixedData.txt", Encoding.Default)
                Do While objReader.Peek() >= 0
                    fileContents = objReader.ReadLine()
                    If fileContents.StartsWith("Bc;") Then
                        intIssuers += 1
                        ReDim Preserve ocdfIssuers(intIssuers)
                        ocdfIssuers(intIssuers) = New OCDFFixedDataIssuers
                        a = Tokenize(fileContents, Seps)
                        For i = LBound(a) To UBound(a) - 1
                            strOCDFBit = Convert.ToString(a(i))
                            If (strOCDFBit.StartsWith("CId")) Then
                                ocdfIssuers(intIssuers).compId = strOCDFBit.Substring("CId".Length)
                            End If
                            If (strOCDFBit.StartsWith("Cn")) Then
                                ocdfIssuers(intIssuers).compName = strOCDFBit.Substring("Cn".Length)
                            End If
                        Next
                    End If
                Loop
                objReader.Close()
            Catch ex As Exception
            End Try
            'TODO Dynamic Array Should be used
            Dim intMessages As Integer = -1
            Dim ocdfMessages() As OCDFMessage = Nothing
            objReader = New StreamReader(FullPath, Encoding.Default)
            Do While objReader.Peek() >= 0
                fileContents = objReader.ReadLine()
                If fileContents.StartsWith("n;") Then
                    intMessages += 1
                    ReDim Preserve ocdfMessages(intMessages)
                    ocdfMessages(intMessages) = New OCDFMessage
                    a = Tokenize(fileContents, Seps)
                    For i = LBound(a) To UBound(a) - 1
                        strOCDFBit = Convert.ToString(a(i))
                        If strOCDFBit.StartsWith("Nh") Then
                            ocdfMessages(intMessages).newsHeader = strOCDFBit.Substring("Nh".Length)
                        ElseIf strOCDFBit.StartsWith("Nl") Then
                            ocdfMessages(intMessages).newsLength = strOCDFBit.Substring("Nl".Length)
                        ElseIf strOCDFBit.StartsWith("Nm") Then
                            ocdfMessages(intMessages).newsMsg = strOCDFBit.Substring("Nm".Length) & vbCrLf
                        ElseIf strOCDFBit.StartsWith("CId") Then
                            ocdfMessages(intMessages).compId = strOCDFBit.Substring("CId".Length)
                        ElseIf strOCDFBit.StartsWith("i") Then
                            ocdfMessages(intMessages).isinCode = strOCDFBit.Substring("i".Length)
                        ElseIf strOCDFBit.StartsWith("OBId") Then
                            ocdfMessages(intMessages).orderBookId = strOCDFBit.Substring("OBId".Length)
                        ElseIf strOCDFBit.StartsWith("Ns") Then
                            ocdfMessages(intMessages).newsSource = strOCDFBit.Substring("Ns".Length)
                        ElseIf strOCDFBit.StartsWith("Nt") Then
                            ocdfMessages(intMessages).newsType = strOCDFBit.Substring("Nt".Length)
                        ElseIf strOCDFBit.StartsWith("NTe") Then
                            ocdfMessages(intMessages).newsTypeEnglish = strOCDFBit.Substring("NTe".Length)
                        ElseIf strOCDFBit.StartsWith("URLn") Then
                            ocdfMessages(intMessages).newsUrl = strOCDFBit.Substring("URLn".Length)
                        ElseIf strOCDFBit.StartsWith("NId") Then
                            ocdfMessages(intMessages).newsMessageId = strOCDFBit.Substring("NId".Length)
                        ElseIf strOCDFBit.StartsWith("t") Then
                            ocdfMessages(intMessages).newsTimestamp = strOCDFBit.Substring("t".Length)
                        ElseIf strOCDFBit.StartsWith("NLa") Then
                            ocdfMessages(intMessages).newsLanguage = strOCDFBit.Substring("NLa".Length)
                        ElseIf strOCDFBit.StartsWith("NCId") Then
                            ocdfMessages(intMessages).newsCorrectionId = strOCDFBit.Substring("NCId".Length)
                        End If
                    Next
                ElseIf intMessages > -1 Then
                    ocdfMessages(intMessages).newsMsg += fileContents & vbCrLf
                End If
            Loop
            ' At the moment we are only receiving one message in each file
            ' Hence the following loop can be discarded in the sence that it will only run once
            For Each ocdfMsg As OCDFMessage In ocdfMessages
                Dim timeStamp As DateTime
                timeStamp = DateTime.ParseExact(ocdfMsg.newsTimestamp, "yyyyMMddHHmmss", Nothing)
                Dim so As String = "OBI-NTB-" & timeStamp.ToString("yyyyMMddHHmmss")
                FullPath = "OBI-" & timeStamp.ToString("yyyyMMdd-HHmmss") & "-" & ocdfMsg.newsMessageId
                Dim intIssuerCounter As Integer = 0
                While (UBound(ocdfIssuers) > intIssuerCounter And ocdfIssuers(intIssuerCounter).compId <> ocdfMsg.compId)
                    intIssuerCounter = intIssuerCounter + 1
                End While

                strConverted = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & vbCrLf _
                & "<!--<!DOCTYPE nitf SYSTEM ""nitf-3-2.dtd"">-->" & vbCrLf _
                & "<nitf version=""-//IPTC//DTD NITF 3.2//EN"" change.date=""October 10, 2003"" change.time=""19:30"" baselang=""no-NO"">" & vbCrLf
                ocdfMsg.newsHeader = ocdfMsg.newsHeader.Replace("\,", ",")
                ocdfMsg.newsHeader = ocdfMsg.newsHeader.Replace("&", "&amp;")
                ocdfMsg.newsHeader = ocdfMsg.newsHeader.Replace("""", "&quot;")
                ocdfMsg.newsHeader = ocdfMsg.newsHeader.Replace("'", "&apos;")
                ocdfMsg.newsHeader = ocdfMsg.newsHeader.Replace("<", "&lt;")
                ocdfMsg.newsHeader = ocdfMsg.newsHeader.Replace(">", "&gt;")
                ocdfMsg.newsHeader = ocdfMsg.newsHeader.Replace("�", "&#8364;")
                If (intIssuerCounter < UBound(ocdfIssuers)) Then
                    ocdfIssuers(intIssuerCounter).compName = ocdfIssuers(intIssuerCounter).compName.Replace("\,", ",")
                    ocdfIssuers(intIssuerCounter).compName = ocdfIssuers(intIssuerCounter).compName.Replace("&", "&amp;")
                    ocdfIssuers(intIssuerCounter).compName = ocdfIssuers(intIssuerCounter).compName.Replace("""", "&quot;")
                    ocdfIssuers(intIssuerCounter).compName = ocdfIssuers(intIssuerCounter).compName.Replace("'", "&apos;")
                    ocdfIssuers(intIssuerCounter).compName = ocdfIssuers(intIssuerCounter).compName.Replace("<", "&lt;")
                    ocdfIssuers(intIssuerCounter).compName = ocdfIssuers(intIssuerCounter).compName.Replace(">", "&gt;")
                    ocdfIssuers(intIssuerCounter).compName = ocdfIssuers(intIssuerCounter).compName.Replace("�", "&#8364;")
                    strConverted &= MakeNITF32Header(FullPath, timeStamp, ocdfIssuers(intIssuerCounter).compName & ": <BR />" & ocdfMsg.newsHeader, so, ocdfMsg.newsLength, iptc, "OBI", "obi")
                Else
                    strConverted &= MakeNITF32Header(FullPath, timeStamp, ocdfMsg.newsHeader, so, ocdfMsg.newsLength, iptc, "OBI", "obi")
                End If
                '+ "<head>" & vbCrLf _
                '+ "<title>" + ocdfMsg.newsHeader + "</title>" & vbCrLf _
                '+ "<meta name=""timestamp"" content=""" + timeStamp.ToString("yyyy.MM.dd HH:mm:ss") + """ />" & vbCrLf _
                '+ "<meta name=""subject"" content="""" />" & vbCrLf _
                '+ "<meta name=""filename"" content=""" + Path.GetFileName(FullPath) + """ />" & vbCrLf _
                '+ "<meta name=""NTBTjeneste"" content=""Formidlingstjenester"" />" & vbCrLf
                strConverted &= "<body>" & vbCrLf _
                + "<body.head>" & vbCrLf _
                + "<hedline><hl1>"
                If (intIssuerCounter < UBound(ocdfIssuers)) Then
                    strConverted &= ocdfIssuers(intIssuerCounter).compName & ": <BR />"
                End If
                strConverted &= ocdfMsg.newsHeader & "</hl1></hedline>" & vbCrLf _
                + "<distributor><org>NTB</org></distributor>" & vbCrLf _
                + "</body.head>" & vbCrLf _
                + "<body.content>" & vbCrLf _
                + "<p lede=""true"">" & ocdfMsg.newsHeader & "</p>" & vbCrLf
                If (ocdfMsg.newsMsg.StartsWith("\" & vbCrLf & "\" & vbCrLf)) Then
                    ocdfMsg.newsMsg = ocdfMsg.newsMsg.Substring(("\" & vbCrLf & "\" & vbCrLf).Length)
                End If
                ocdfMsg.newsMsg = ocdfMsg.newsMsg.Replace("\,", ",")
                ocdfMsg.newsMsg = ocdfMsg.newsMsg.Replace("&", "&amp;")
                ocdfMsg.newsMsg = ocdfMsg.newsMsg.Replace("""", "&quot;")
                ocdfMsg.newsMsg = ocdfMsg.newsMsg.Replace("'", "&apos;")
                ocdfMsg.newsMsg = ocdfMsg.newsMsg.Replace("<", "&lt;")
                ocdfMsg.newsMsg = ocdfMsg.newsMsg.Replace(">", "&gt;")
                ocdfMsg.newsMsg = ocdfMsg.newsMsg.Replace("�", "&#8364;")
                ocdfMsg.newsMsg = System.Text.RegularExpressions.Regex.Replace(ocdfMsg.newsMsg, "(http[s]?://[^\s]+)", "<a href=""$1"" target=""_blank"">$1</a>")
                strConverted &= "<p>" & ocdfMsg.newsMsg.Replace("\" & vbCrLf & "\" & vbCrLf, vbCrLf & "</p>" & vbCrLf & "<p>" & vbCrLf).Replace("\" & vbCrLf, vbCrLf) & "</p>" & vbCrLf
                If (Not ocdfMsg.newsUrl Is Nothing) Then
                    If (Not ocdfMsg.newsUrl = "") Then
                        strConverted &= "   <media media-type=""url"" class=""prs"">"
                        strConverted &= "       <media-caption>"
                        strConverted &= "           <a href=""" & ocdfMsg.newsUrl & """ target=""_blank"">" & ocdfMsg.newsUrl & "</a>"
                        strConverted &= "       </media-caption>"
                        strConverted &= "   </media>"
                    End If
                End If
                strConverted &= "</body.content>" & vbCrLf _
                + "</body>" & vbCrLf _
                + "</nitf>" & vbCrLf

                'Save as single file
            Next

            Return strConverted
        Catch ex As Exception
            ErrInfo = ex.Message
        Finally
            objReader.Close()
        End Try
    End Function
    Function Tokenize(ByVal TokenString As String, ByRef TokenSeparators() As String) As String()

        Dim NumWords As Integer
        Dim a() As String
        NumWords = 0

        Dim NumSeps As Integer
        Dim SepIndex As Integer
        Dim SepPosition As Integer
        NumSeps = UBound(TokenSeparators)

        Do

            SepPosition = 0
            SepIndex = -1
            Dim i As Integer
            For i = 0 To NumSeps - 1

                ' Find location of separator in the string

                Dim pos As Integer = 0
                pos = InStr(TokenString, TokenSeparators(i))

                ' Is the separator present, and is it closest to the beginning of the string?

                If pos > 0 And ((SepPosition = 0) Or (pos < SepPosition)) Then
                    SepPosition = pos
                    SepIndex = i
                End If

            Next

            ' Did we find any separators?	

            If SepIndex < 0 Then

                ' None found - so the token is the remaining string

                ReDim Preserve a(NumWords + 1)
                a(NumWords) = TokenString

            Else

                ' Found a token - pull out the substring		

                Dim substr As String
                substr = Trim(Left(TokenString, SepPosition - 1))

                ' Add the token to the list

                ReDim Preserve a(NumWords + 1)
                a(NumWords) = substr

                ' Cutoff the token we just found

                Dim TrimPosition As Integer
                TrimPosition = SepPosition + Len(TokenSeparators(SepIndex))
                TokenString = Trim(Mid(TokenString, TrimPosition))

            End If

            NumWords = NumWords + 1
        Loop While (SepIndex >= 0)

        Tokenize = a

    End Function
    Private Shared Function MakeNITF32Header(ByVal fn As String, ByVal dt As DateTime, ByVal title As String, ByVal so As String, ByVal length As Integer, ByVal iptc As Integer, ByVal msgSource As String, ByVal msgSign As String) As String

        Dim output As StringBuilder = New StringBuilder

        Dim id As String = msgSource & ":NTB:" & dt.ToString("yyyyMMdd:mmss:") & length Mod 1000

        output.Append("<head>" & vbCrLf)
        output.Append("<title>" & title & "</title>" & vbCrLf)

        output.Append("<meta name=""timestamp"" content=""" & dt.ToString("yyyy.MM.dd HH:mm:ss") & """ />" & vbCrLf)

        output.Append("<meta name=""subject"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""foldername"" content=""Ut-Satellitt"" />" & vbCrLf)
        output.Append("<meta name=""filename"" content=""" & fn & ".xml"" />" & vbCrLf)

        output.Append("<meta name=""NTBTjeneste"" content=""Formidlingstjenester"" />" & vbCrLf)
        output.Append("<meta name=""NTBMeldingsSign"" content=""" & msgSign & "/"" />" & vbCrLf)
        output.Append("<meta name=""NTBPrioritet"" content=""6"" />" & vbCrLf)
        output.Append("<meta name=""NTBStikkord"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""NTBDistribusjonsKode"" content=""ALL"" />" & vbCrLf)
        output.Append("<meta name=""NTBKanal"" content=""C"" />" & vbCrLf)
        output.Append("<meta name=""NTBIPTCSequence"" content=""" & iptc & """ />" & vbCrLf)
        output.Append("<meta name=""NTBID"" content=""" & id & """ />" & vbCrLf)

        output.Append("<meta name=""ntb-dato"" content=""" & dt.ToString("dd.MM.yyyy HH:mm") & """ />" & vbCrLf)

        output.Append("<tobject tobject.type=""PRM-" & msgSource & """>" & vbCrLf)
        output.Append("<tobject.property tobject.property.type=""Fulltekstmeldinger"" />" & vbCrLf)
        output.Append("</tobject>" & vbCrLf)

        output.Append("<docdata>" & vbCrLf)
        output.Append("<doc-id regsrc=""NTB"" id-string=""" & id & """/>" & vbCrLf)
        output.Append("<urgency ed-urg=""6""/>" & vbCrLf)
        output.Append("<date.issue norm=""" & dt.ToString("yyyy-MM-ddTHH:mm:ss") & """/>" & vbCrLf)
        output.Append("<ed-msg info="""" />" & vbCrLf)
        output.Append("<du-key version=""1"" key=""" & so & """ />" & vbCrLf)
        output.Append("<doc.copyright year=""" & dt.ToString("yyyy") & """ holder=""NTB"" /> " & vbCrLf)
        output.Append("<key-list>" & vbCrLf)
        output.Append("<keyword key=""" & so & """ />" & vbCrLf)
        output.Append("</key-list>" & vbCrLf)
        output.Append("</docdata>" & vbCrLf)

        output.Append("<pubdata date.publication=""" & dt.ToString("yyyyMMddTHHmmss") & """ item-length=""" & length & """ unit-of-measure=""character"" />" & vbCrLf)

        output.Append("<revision-history name=""" & msgSign & "/"" />" & vbCrLf)

        output.Append("</head>" & vbCrLf)

        Return output.ToString()

    End Function

End Class
