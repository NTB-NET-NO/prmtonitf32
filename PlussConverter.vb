Imports ntb_FuncLib
Imports System.io
Imports System.Text
Imports System.Text.RegularExpressions

Public Class PlussConverter

    Private xsltConverter As MSXML2.IXSLProcessor


    Public Sub New(ByVal convertXSLTFile As String)

        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
        Dim xmlError As MSXML2.IXMLDOMParseError
        Dim xslt As MSXML2.XSLTemplate

        xslt = New MSXML2.XSLTemplate
        xsltDoc = New MSXML2.FreeThreadedDOMDocument

        xsltDoc.load(convertXSLTFile)
        xmlError = xsltDoc.parseError

        xslt.stylesheet = xsltDoc
        xsltConverter = xslt.createProcessor

    End Sub

    Function ConvertPlussFile2NITF(ByVal file As String, ByVal iptc As Integer) As String

        Dim xml4Doc As MSXML2.DOMDocument = New MSXML2.DOMDocument

        xml4Doc.load(file)
        xsltConverter.addParameter("filename", Path.GetFileName(file))
        xsltConverter.addParameter("iptc_seq", iptc)
        xsltConverter.input = xml4Doc

        xsltConverter.transform()

        Return Replace(xsltConverter.output, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)

    End Function

End Class
