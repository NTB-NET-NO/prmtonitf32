Imports System.ComponentModel
Imports System.Configuration.Install

<RunInstaller(True)> Public Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Installer overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PRMToNITF32ServiceProcessInstaller As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents PRMToNITF32ServiceInstaller As System.ServiceProcess.ServiceInstaller
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PRMToNITF32ServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller
        Me.PRMToNITF32ServiceInstaller = New System.ServiceProcess.ServiceInstaller
        '
        'PRMToNITF32ServiceProcessInstaller
        '
        Me.PRMToNITF32ServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.PRMToNITF32ServiceProcessInstaller.Password = Nothing
        Me.PRMToNITF32ServiceProcessInstaller.Username = Nothing
        '
        'PRMToNITF32ServiceInstaller
        '
        Me.PRMToNITF32ServiceInstaller.DisplayName = "NTB_PRMToNITF32"
        Me.PRMToNITF32ServiceInstaller.ServiceName = "NTB_PRMToNITF32"
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.PRMToNITF32ServiceProcessInstaller, Me.PRMToNITF32ServiceInstaller})

    End Sub

#End Region

    Private Sub ServiceProcessInstaller1_AfterInstall(ByVal sender As System.Object, ByVal e As System.Configuration.Install.InstallEventArgs) Handles PRMToNITF32ServiceProcessInstaller.AfterInstall

    End Sub
End Class
